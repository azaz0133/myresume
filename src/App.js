import React from 'react'
import {
  compose,
  lifecycle,
  withHandlers,
  withState
} from 'recompose'
import './common/style.css'
import {
  firebase,
  config
} from './firebase.config.js'
import { 
  Skills,
  RecentWork,
  Portfolios,
  Information,
  Interested
} from './modules'

const App = ({
 store,isLoading
}) => (
   <div className="container">
     <p className="title is-1 is-spaced has-text-light" style={{textAlign:'center'}}>My Resume</p>
     <br />
     <Information isLoading={isLoading} />
     <div className="columns">
        <div className="column is-three-fifths">
          <Skills data={store['skillData']} isLoading={isLoading} db={firebase} />
        </div>
        <div className="column">
        <Interested data={store['interestedData']} isLoading={isLoading} db={firebase}/>           
        </div>
     </div>              
     <Portfolios data={store['portData']} isLoading={isLoading} db={firebase}/>
     <RecentWork data= {store['recentWorkData']} isLoading={isLoading} db={firebase}/>    
   </div>
)

export default compose(
  withState("store","setStore",{
    interestedData:{},
    recentWorkData:{},
    skillData:{},
    portData:{}
  }),
  withState("isLoading","setLoading",true),
  withHandlers({
    handlerSnapshot: (props) => async e => {
      firebase.database().ref().on('value',snapshot => {
       const data = snapshot.val()
       const interestedData = data['Interested']
       const recentWorkData = data['RecentWork']
       const skillData = data['Skills']
       const portData = data['Portfolio']
       props.setStore({
        interestedData,
        recentWorkData,
        skillData,
        portData,
       })
      })
       
    }
  }),
  lifecycle({
    componentDidMount(){
       firebase.initializeApp(config)
        this.props.handlerSnapshot()
        setTimeout(_ => this.props.setLoading(false),2000)
    }
  })
)(App)