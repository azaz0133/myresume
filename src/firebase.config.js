import firebase from 'firebase'

const config = {
    apiKey: "AIzaSyD_q661xjE06rX6tp34Mgghr5MWAXpilXQ",
    authDomain: "resumeonline-28ac2.firebaseapp.com",
    databaseURL: "https://resumeonline-28ac2.firebaseio.com",
    projectId: "resumeonline-28ac2",
    storageBucket: "resumeonline-28ac2.appspot.com",
    messagingSenderId: "1066537918494"
}

 function initialFirebase(){
    return firebase.initializeApp(config)
}
export {
    initialFirebase,
    firebase,
    config
}