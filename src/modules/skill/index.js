import React from 'react'
import {
    compose,
    withHandlers,
    lifecycle,
    withState
} from 'recompose'
import Skeleton from 'react-loading-skeleton'
import {Table,ModalForm} from './components'
import shallowCompare from 'react-addons-shallow-compare'

const Skills = ({
  skills,isLoading,closeModal,isOpen,openModal,handlerSubmit,handlerUpdate,db
}) => (
    <div>
        <article className="message">
                <div className="message-header">
                    <p>My Skill</p>
                </div>
               
                <div className="message-body">
                    <div className="content">
                    {isLoading ? <Skeleton count={10}/>:
                        <Table skills={skills} update={handlerUpdate} isOpen2={isOpen} db={db}/>                            
                    } 
                    </div>
                    <a className="ico" onClick={openModal}></a>
                </div>
        </article>
       <ModalForm isOpen={isOpen} onClose={closeModal} submit={handlerSubmit}/>
    </div>
)


export default compose(
    withState("skills","setSkill",[]),
    withState("isOpen","setOpen",false),
    withHandlers({
        dispathData: (props) => (data) => {
            const keySkill = Object.keys(data).map(key => ({...data[key],key}))
            if(props.skills.length !== keySkill.length) props.setSkill(keySkill)
        },
        openModal:({setOpen}) => () => {
            setOpen(true)
        },
        closeModal:({setOpen}) => () => {
            setOpen(false)
        },
        handlerSubmit: (props) => (data,e) => {
             e.preventDefault()
             props.db.database().ref(`Skills/${data['name']}`).set({
                 name: data['name'],
                 description: data['description'],
                 level: data['level'],
                 exampleProject: data['exampleProject']
             }).then( ref => props.setOpen(false)).catch(err => console.log(err))
        }
    }),
    lifecycle({
        componentWillReceiveProps(nextProps){
            this.props.dispathData(nextProps.data)
        },
        shouldComponentUpdate(nextProps,nextState){
            return shallowCompare(this, nextProps, nextState);        
        }
    })
)(Skills)