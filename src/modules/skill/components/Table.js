import React from 'react'
import { compose, withState,withHandlers } from 'recompose';
import Modal from './ModalForm'

const Table = ({
    skills,isOpen,closeModal,openModal,form,handlerUpdate,handlerDelete
   }) => (
       <table>
           <thead>
               <tr>
               <th><abbr title="Name"></abbr>Name</th>
               <th><abbr title="Description"></abbr>Description</th>
               <th><abbr title="Level"></abbr>Level</th>
               <th><abbr title="Example project"></abbr>Example project</th>
               </tr>
           </thead>
           <tbody>
               {
                   skills.map(data => 
                       <tr key={data.name} >
                           <th><abbr title="Name"></abbr>{data.name}</th>
                           <th><abbr title="Description"></abbr>{data.description}</th>
                           <th><abbr title="Level"></abbr>{data.level}</th>
                           <th><abbr title="Example project"></abbr> {data.exampleProject !== "#"? <a href={data.exampleProject}>Click</a> :"Empty"} </th>  
                           <a className="edit" onClick={e => openModal(data,e)}></a>
                       </tr>
                     
                   )
               }
           </tbody>
           <Modal   data={form} isOpen={isOpen} onClose={closeModal} update={handlerUpdate} handlerDelete={handlerDelete} />
     </table>
   )

   export default compose(
       withState('form','setForm',{
        name:"",
        description:"",
        exampleProject:"",
        level:"1/10"
       }),
       withState("isOpen","setOpen",false),
       withHandlers({
        openModal:({setOpen,setForm}) => (data,e) => {
                e.preventDefault()
                setForm(data)
                    setOpen(true)
        },
        closeModal:({setOpen}) => () => {
            setOpen(false)
        },
        handlerUpdate: props => (data,e) => {
            e.preventDefault()
             props.db.database().ref(`Skills/${data['key']}`).set({
                ...data
            },(err)=>{
                if(err) console.log(err)
                else {
                    props.setOpen(false)
                    console.log("done")
                }
            })
        },
        handlerDelete: props => (data,e) => {
            e.preventDefault()
            props.db.database().ref(`Skills/${data['key']}`).remove().then(() => props.setOpen(false)).catch(err => console.log(err))

        }
    })
   )(Table)