import React from 'react'
import Modal from 'react-responsive-modal';
import { compose, withState, withHandlers, lifecycle } from 'recompose';

const ModalForm = ({
    isOpen,onClose,submit,onChange,form,data,update,handlerDelete
  }) => (
      <Modal open={isOpen} onClose={onClose} center>
          <div className="field">
              <label className="label">Name</label>
              <div className="control">
                  <input className="input" type="text" placeholder="e.g Alex Smith" onChange={onChange} name="name" defaultValue ={data.name}/>
              </div>
          </div>
  
          <div className="field">
              <label className="label">Description</label>
              <div className="control">
                  <textarea className="textarea" placeholder="Textarea" onChange={onChange} name="description" defaultValue ={data.description}></textarea>
              </div>
          </div>
  
          <div className="field">
              <label className="label">Level</label>
              <div className="control">
                  <div className="select">
                  <select onChange={onChange} name="level" defaultValue ={data.level}>
                      {
                          ["1/10","2/10","3/10",
                          "4/10","5/10","6/10",
                          "7/10","8/10","9/10",
                          "10/10"].map( level =>
                                  <option key={new Date()} value={level}>{level}</option>
                              )
                      }
                  </select>
                  </div>
              </div>
          </div>
          <div className="field">
              <label className="label">example project</label>
              <div className="control">
                  <input className="input"  onChange={onChange} name="exampleProject"  defaultValue ={data.exampleProject} />
              </div>
          </div>
          {
              data.name.length == 0 &&  <a href="#" className="button is-success is-outlined" onClick={e =>submit(form,e)}>Submit</a>
          }
          {
              data.name.length > 0 && <a href="#" className="button is-success is-outlined" onClick={e => update(form,e)}>Edit</a>
          }
          {' '}
          {
              data.name.length > 0 && <a href="#" className="button is-danger is-outlined" onClick={e => handlerDelete(data,e)}>Delete</a>
          }

      </Modal>
  )


 ModalForm.defaultProps = {
     form :{
        name:"",
        description:"",
        exampleProject:"",
        level:"1/10"
    },
    data: {
        name:"",
        description:"",
        exampleProject:"",
        level:"1/10"
    }
 }

  export default compose(
      withState("form","setForm",{
        name:"",
        description:"",
        exampleProject:"",
        level:"1/10"
      }),
      withHandlers({
          onChange: props => ({target:{name,value}}) => {
             if(!!props.data){
                 props.setForm({
                     ...props.data,
                     [name]:value
                 })
             }
             else {
                props.setForm({
                    ...props.form,
                    [name]:value
                })
             }
          }
      }),
      lifecycle({

      })
  )(ModalForm)
