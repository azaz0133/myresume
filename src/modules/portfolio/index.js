import React from 'react'
import { compose, withHandlers, lifecycle, withState } from 'recompose';
import { Portfolio  } from './components'
import Modal from 'react-responsive-modal'
import shallowCompare from 'react-addons-shallow-compare'

const Portfolios = ({
    port,isLoading,form,onChange,editPortfolio,
    isOpen,setOpen,createPortfolio,deletePortfolio
}) => <div>
        <h1 className="title is-1" style={{textAlign:'right'}}>
        Portfolio and Activity  
        <a className="ico" style={{marginLeft:"auto"}} onClick={() => setOpen(true)}></a>
        </h1>
         {
             port.map((data,index) => {
                 let style ={
                    flexDirection: "row-reverse"
                 }
                 if(index%2!=0){
                    style = {
                        flexDirection:"initial"
                    }
                 }
                return <Portfolio key ={data.name} data={data} style={style} isLoading={isLoading} edit={editPortfolio} onDelete={deletePortfolio} />
             })
         }
         <Modal open={isOpen} onClose={() => setOpen(false)}>
         <div className="field">
            <div className="control">
                <label className="label" >name</label>
                <input className="input is-info" type="text" placeholder="Name" name="name" onChange={onChange}/>
            </div>
         </div>
         <div className="field">
            <div className="control">
                <label className="label" >description</label>
                <input className="input is-info" type="text" placeholder="Description" name="description" onChange={onChange}/>
            </div>
         </div>
         <div className="field">
            <div className="control">
                <label className="label" >date</label>
                <input className="input is-info" type="text" placeholder="Info input" name="date" onChange={onChange}/>
            </div>
            <a className="button is-success is-outlined" onClick={createPortfolio}>Submit</a>
         </div>
         </Modal>
      </div>


export default compose(
    withState('port','setPort',[]),
    withState('form',"setForm",{
        name:"",
        description:"",
        date:""
    }),
    withState('isOpen','setOpen',false),
    withHandlers({
        dispathData: (props) => (data) => {
            const keyPort = Object.keys(data).map(key => ({...data[key],key}))
            if(props.port.length !== keyPort.length) props.setPort(keyPort)
        },
        onChange: props => ({target:{name,value}}) => {
            props.setForm({
                ...props.form,
                [name]:value
            })
        },
        createPortfolio: ({form,db,setOpen}) => (e) => {
            e.preventDefault()
            db.database().ref(`Portfolio/${form['name']}`).set({
                name: form['name'],
                description: form['description'],
                date:form['date'],
                images:""
            }).then( ref => setOpen(false)).catch(err => console.log(err))
        },
        editPortfolio: props => data => {
            props.db.database().ref(`Portfolio/${data['key']}`).set({
                ...data
            },(err)=>{
                if(err) console.log(err)
                else {
                    console.log("done")
                }
            })
        },
        deletePortfolio: props => data => {
            console.log(data);
            props.db.database().ref(`Portfolio/${data['key']}`).remove().then(() => props.setOpen(false)).catch(err => console.log(err))
        }
    }),
    lifecycle({
        componentWillReceiveProps(nextProps){
            this.props.dispathData(nextProps.data)
        },
        shouldComponentUpdate(nextProps,nextState){
            return shallowCompare(this, nextProps, nextState);        
        }
    })
)(Portfolios)