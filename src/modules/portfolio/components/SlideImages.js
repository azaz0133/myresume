import React from 'react'
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';

const SlideImages = ({
 image
}) => (
    <Carousel 
        width="350px"
        dynamicHeight={true}
        autoPlay={true}
        infiniteLoop={true}
    >
        { image !== "" ? Object.keys(image).map(key => 
            <div key={key} >
                <img alt={key} src={image[key]} />
                <p className="legend">{key}</p>
           </div>) : 
             <div>
                <img alt="empth image" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQjNrDQGPIUPUOtGdSy17h6taL_gk3LiOmpciZf4wEkn_dhUIOX" />
                <p className="legend">No Image</p>
            </div>
        }
        
    </Carousel>
)

export default SlideImages