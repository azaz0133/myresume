import React from 'react'
import {PulseLoader} from 'react-spinners'
import Skeleton from 'react-loading-skeleton'
import { SlideImages } from '.'
import { compose, withState, withHandlers, lifecycle } from 'recompose';
import Modal from 'react-responsive-modal'

const ModalEdit = ({
    isOpen,onChange,edit,setOpen,form,del
}) => (
    <Modal open={isOpen} onClose={() => setOpen(false)}>
         <div className="field">
            <div className="control">
                <label className="label" >name</label>
                <input className="input is-info" type="text" placeholder="Name" name="name" onChange={onChange} defaultValue={form.name}/>
            </div>
         </div>
         <div className="field">
            <div className="control">
                <label className="label" >description</label>
                <input className="input is-info" type="text" placeholder="Description" name="description" onChange={onChange} defaultValue={form.description}/>
            </div>
         </div>
         <div className="field">
            <div className="control">
                <label className="label" >date</label>
                <input className="input is-info" type="text" placeholder="Info input" name="date" onChange={onChange} defaultValue={form.date}/>
            </div>
            <a className="button is-success is-outlined" onClick={() => { 
                setOpen(false) 
                return edit(form)
                }}>Edit</a>
            <a className="button is-danger is-outlined" onClick={() => { 
                setOpen(false) 
                return del(form)
                }}>Delete</a>
         </div>
    </Modal>
)

const Port = ({
    data,style,isLoading,deletePortfolio,setOpen,isOpen,onChange,edit,form,onDelete
}) => (
    <div className="columns is-mobile" style={style}>
      <div className="column is-three-quarters" style={{marginTop:"5%"}}>
      <button className="delete" aria-label="delete" onClick={() => setOpen(true)}></button>                
            <div className="content is-medium">
                <article className="message">
                    <div className="message-header">
                    { isLoading ? <Skeleton /> : <p>{data.name}</p> }
                    </div>
                    <div className="message-body">
                    { isLoading ? <Skeleton count={2} /> :
                     <p>
                        <small>Date : {data.date}</small><br/>
                        {data.description}
                     </p>
                    }
                    </div>
                    <ModalEdit isOpen={isOpen} setOpen={setOpen} onChange={onChange} form={form} edit={edit} del={onDelete} />
                </article>
            </div> 
      </div>
      <div className="column">
      {
           isLoading ? <PulseLoader 
                        sizeUnit={"px"}
                        size={100}
                        color={'#123abc'}
                        /> : 
                        <SlideImages image={data.images} />
       }
      </div>
    </div>
)

export default compose(
    withState('isOpen',"setOpen",false),
    withState('form','setForm',{}),
    withHandlers({
        onChange: props => ({target:{name,value}}) => {
            props.setForm({
                ...props.form,
                [name]:value
            })

        }
    }),
    lifecycle({
        componentDidMount(){
             this.props.setForm({
                 ...this.props.data
             })
        }
    })
)(Port)