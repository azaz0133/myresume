import React from 'react'
import {
 compose,
 withHandlers,
 withState,
 lifecycle
} from 'recompose'

const Card = ({
    data
}) => (
    <div className="column">
    <div className="box">
        <div className="media">
            <div className="media-left">
            <figure className="image is-64x64">
                <img src={data.logo} alt={data.name} />
            </figure>
            </div>
            <div className="media-content">
            <div className="content">
                <p>
                <strong>{data.name}</strong> <br/>
                <small>Date Employed: {` ${data.startDate}`}</small> <small>Date Leaving: {` ${data.endDate}`}</small>
                <br/>
                  Position: {data.position} <br/>
                  Description: {data.jobDescription}<br/>
                  Technology what used in work: {data.technologies}
                </p>
            </div>
            <nav className="level is-mobile">
                <div className="level-left">
                <a className="level-item" aria-label="reply">
                    <span className="icon is-small">
                    <i className="fas fa-reply" aria-hidden="true"></i>
                    </span>
                </a>
                <a className="level-item" aria-label="retweet">
                    <span className="icon is-small">
                    <i className="fas fa-retweet" aria-hidden="true"></i>
                    </span>
                </a>
                <a className="level-item" aria-label="like">
                    <span className="icon is-small">
                    <i className="fas fa-heart" aria-hidden="true"></i>
                    </span>
                </a>
                </div>
            </nav>
            </div>
        </div>
     </div>
    </div>   
)


const RecentWork =({
    companies
}) => (
    <div className="columns">
        {
            companies.map(data => <Card key={data.name} data={data} />)
        }
    </div>
)


export default compose(
    withState('companies','setCompanies',[]),
    withHandlers({
        dispathData: (props) => (data) => {
            const keyCompany = Object.keys(data).map(key => data[key])
            if(props.companies.length !== keyCompany.length) props.setCompanies(keyCompany)
        }
    }),
    lifecycle({
        componentWillReceiveProps(nextProps){
            this.props.dispathData(nextProps.data)
        }
    })
)(RecentWork)