import React from 'react'


const Interested = ({
  data
}) => (
    <article className="message">
    <div className="message-header">
        <p>Interested in</p>
        <button className="delete" aria-label="delete"></button>
    </div>
 
    <div className="message-body">
        <ol type="1">
        {
            Object.keys(data).map( key => 
                <li key={key}>{key}</li>
            )
        }
        </ol>
  
    </div>
</article>
)


export default (Interested)