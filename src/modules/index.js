export {default as Skills } from './skill'
export {default as RecentWork } from './recentWork'
export {default as Portfolios } from './portfolio'
export {default as Information } from './information'
export {default as Interested } from './interested'

