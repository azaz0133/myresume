import React from 'react'
import Skeleton from 'react-loading-skeleton'

const Information = ({
    isLoading
}) => (
    <div  style={{
        width:"100%",
        height:"50%",
    }} >
        <div className="columns">
            <div className="column">
                <figure className="image">
                    <img className="is-rounded" 
                    src="https://scontent.fbkk2-7.fna.fbcdn.net/v/t1.0-9/14045886_924079394405276_7472425123064237642_n.jpg?_nc_cat=111&_nc_ht=scontent.fbkk2-7.fna&oh=d8e759233524688243bce732a4564a8e&oe=5C998FE2" 
                    style={{
                        width:"50%",
                        margin: "10% 30%"
                    }}
                    />
                </figure>
            </div>
            <div className="column">
            <article className="message is-large is-info" style={{
                margin:"10% 0 0 0",
            }}>
                <div className="message-header">
                    Profile
                </div>
               {isLoading ? <Skeleton count={8} /> :
                    <p style={{paddingLeft:"5%"}}>
                    Name: Anirut Kamchai<br/>
                    ชื่อ: อนิรุทธิ์ คำชาย <br/>
                    Age: 21 <br/>
                    Birthdate: 29/07/1997 <br/>
                    Bachelor Degrees <br/>
                    Faculty of Science <br/>    
                    Major: Computer-Science <br/>
                    GRADE(Latest): 3.41       
                </p>
                }  
            </article>
            </div>
        </div>
    </div>
)

export default Information